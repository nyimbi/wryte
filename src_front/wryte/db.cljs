(ns wryte.db
  (:require [reagent.core :as reagent :refer [atom]]
            [cljs.test :refer-macros [is deftest]]
            [alandipert.storage-atom :refer [local-storage]]
            [clojure.string :refer [join]]))

;; helper function for templates
(defn ddd [& args]
  true)

(defn log [x] (.log js/console x))

(enable-console-print!)

;; =================
;; Data definitions:

;; Paragraph is one of
;; - [:p "String"]
;; - nil
;; interp. as a hiccup paragraph with text till linebreak

(def p1 [:p "Das ist ein string"])
(def p2 nil)

#_
(def fn-for-paragraph [p]
  (if (p)
    (ddd p)
    (ddd p)))

;; ListOfParagraph is one of:
;; - []
;; - (conj ListOfParagraph Paragraph)
;; interp. as a List of Hiccup paragraphs with text

;; SwitchState is one of:
;; -true
;; -false
;; interp. if a recording function is active or not

(def onoff1 false)
(def onoff2 true)

#_
(defn fn-for-switchstate [ss]
  (if ss
    (ddd ss)
    (ddd ss)))


;; Position is Natural
;; interp. as the position of the cursor

(def pos1 0)
(def pos2 365)

#_
(defn fn-for-position p
  (ddd p))


;; Result is one of:
;; - hicup
;; -- hiccup a combination of:
;; -- symbol
;; -- ListOfParagraphs
;; - []
;; interp. the  final or tempory result of an dictation

(def result nil)
(def sel2 [:p "Bla bla bla"])


;; Recognition is on of:
;; - nil
;; - SpeechRecognition object
;; interp. as the the windows SpeechRecognition object

(def recog1 (new js/window.webkitSpeechRecognition))
(def recog2 nil)

;; Punctuation is on of:
;; - .
;; - ,
;; - ?
;; - !
;; - ;
;; - -
;; - "
;; - '
;; - ...
;; interp. as a punctuation mark that is set via user unteraction (click, key)

(def punc1 ".")


#_
(defn [p]
  (if (in p ["." "," "?" "!" "-" "\"" "'"]))
  (ddd)
  (ddd))

;; {:tag :error
;;  :attributes
;;  {
;;    :contextoffset 6
;;    :category "Mögliche Tippfehler"
;;    :toy 0
;;    :offset 6
;;    :replacements "Frau#-frau#Drau#flau#frag#fraß#grau#rau#trau#Bau#frei#Irak#Iran#Grad#Traum#drauf#früh#raus#freue#froh"
;;    :msg "Möglicher Rechtschreibfehler gefunden"
;;    :tox 10
;;    :errorlength 4
;;    :ruleId GERMAN_SPELLER_RULE
;;    :context "Hallo frau lechner,  leider können wir ihren linux..."
;;    :fromy 0
;;    :locqualityissuetype "misspelling"
;;    :fromx 6}
;;  :content []))


;; {:recording? false :edit? true :checked? true
;;  :iresult "" :fresult "" :pos 0 :punc ""}
;; GlobalState is of:
;; - SwitchState recording
;; - SwitchState edit or dictate view
;; - SwitchState checked? : fresult is spell checked
;; - Result for interim result
;; - Result for final result
;; - Pos is current cursor position
;; - Recognition is the SpeechRecognition object
;; - Punc is the punctiation mark set after next final result

#_
(defn fn-for-recognition [r]
  (if r
    (ddd r)
    (ddd r)))

;; =====================
;; Default app-db Value:

;; When the application first starts, this will be the value put in app-db

(def fr [:div [:p "Test mit langem gelaber und was weiß ich"][:p "Fetter zweiter absatz"]])

(def app-db (local-storage
             (atom {
                    :recording? false   ;; not recording
                    :edit? false        ;; not editing
                    :checked? true      ;; doesn't need to get checked on start
                    :iresult ""         ;; empty interim result
                    :fresult [:div [:p ""]]
                              ; [:p "Dies ist ein Beispiel-Text, um zu zeigen, wie LanguageTool funktioniert. Wie man sieht, ist auch eine " [:span {:class "error" :data-alternatives "Rächer,Rechthaber,Rechtsstaat" :data-replaced "Rächtshreibprüfung"} "Rechtschreibprüfung"] " enthalten."]
                              ; [:p "Wieso sind gerade in dem kleinen mittelamerikanischen Land so viele " [:span {:class "error" :data-alternatives "Bla,Bla" :data-replaced "Briefkastenfirmen"} "Briefkastenförmen"] " entstanden?"]]         ;; empty final result
                    :pos 0              ;; cursor position at start of textarea
                    :punc ""            ;; no punctuation needed at start
                    :ignore {"onvista" "OnVista" "casual" "Casual"
                             "dating" "Dating" "auxmoney" "auxmoney"
                             "flatex" "flatex"
                             "Leister und Zubehör" "Lifestyle-Entrepreneur"} ;; words that should be ignored by spell checker
                    :language :deutsch})
             :db))

;(alandipert.storage-atom/clear-html-storage!)
(alandipert.storage-atom/clear-local-storage!)

(def webspeech (atom js/Object))

(def languages {:english "en-US" :deutsch "de-DE" :afrikaans "af-ZA" :espanol "es-AR" :francais "fr-FR" :italiano "it-IT" :portugues "pt-BR" :turkce "tr-TR"})

;;=========
;; Helpers:

(defn- swap-state-elm!
  "Keyword StateElement GlobalState -> GlobalState
  Consumes state element and new value, swaps for new value, produces the GlobalState"
  [ky val gs]
  (swap! gs assoc ky val))

#_
(let [test-db (atom (global-state. false false true "" "" 0 nil ""))]
  (is (= (swap-state-elm! :recording? true test-db)
         (atom (global-state. true false true "" "" 0 nil ""))))
  (is (= (swap-state-elm! :iresult "test" test-db)
         (atom (global-state. true false "" "test" 0 nil ""))))
  (is (= (swap-state-elm! :pos 100 test-db)
         (atom (global-state. true false "" "test" 100 nil "")))))

(defn- set-punc!
  "Punc GlobalState -> GlobalState
  Consumes a Punctuation and a GlobalState, swaps the Punctuation  and produces the new GlobalState"
  [p gs]
  (swap-state-elm! :punc p app-db))

(defn- pgraph->string
  "Hiccup pgraph -> String
  Consumes a paragraph with spans and produces a string for that paragraph"
  [p]
  (cond
    (empty? p)          ""
    (string? (first p)) (str (first p)        (pgraph->string (rest p)))
    (vector?    (first p)) (str (last (first p)) (pgraph->string (rest p)))
    :else                                     (pgraph->string (rest p))))
(println "P->S1 " (pgraph->string []))
(println "P->S2 " (pgraph->string [:p]))
(println "P->S3 " (pgraph->string [:p {}]))
(println "P->S4 " (pgraph->string [:p {} "Test"]))
(println "P->S5 " (pgraph->string [:p {} "Test " [:span "Span"]]))
(println "P->S6 " (pgraph->string [:p {} "Test " [:span "Span"] " test"]))

(defn fresult->string
  "Result -> String
  Consume a Result Vector r and produces a String to send to languagetool"
  [r]
  (let [pgraphs (rest r)
        strings (map pgraph->string pgraphs)]
    (join "\n" strings)))
(is (= "First paragraph\n\nSecond paragraph"
     (fresult->string [:div [:p "First paragraph"] [:p "Second paragraph"]])))
(is (= "First paragraph\n\nSecond paragraph with Span")
    (fresult->string [:div [:p "First paragraph"] [:p "Second paragraph with " [:span "Span"]]]))
(println "FRESULT->STRING: " (fresult->string [:div [:p "First paragraph " [:span "rnr neanr aerrn iter"]] [:p "Second paragraph with " [:span "Span"]]]))
;;==============
;; DB-Functions:

(defn recording?
  "Void -> SwitchState
  Produces the state of recording?"
  []
  (:recording? @app-db))

(defn edit?
  "Void -> SwitchState
    Produces the state of edit?"
  []
  (:edit? @app-db))

(defn checked?
  "Void -> SwitchState
    Produces the state of edit?"
  []
  (:checked? @app-db))

(defn iresult []
  "Void -> Result
  Produces the interim result"
  (:iresult @app-db))


(defn fresult
  "Void -> Result
  Produces the final result"
  []
  (:fresult @app-db))

(defn punc
  "Void -> Punctuation
  Produces the curren punctuation sign if any"
  []
  (:punc @app-db))

(defn ignore-list
  "Void -> ListOfString
  Produces the current ignore-list"
  []
  (:ignore @app-db))

(defn language
  "Void -> Keyword
  Produces the current language"
 []
 (:language @app-db))

(defn locale
  "Void -> String
   Produces the locale string for the current language"
  []
  ((language) languages))

(defn pos
  "Void -> String
  Produces the position in the editor"
  []
  (:pos @app-db))

(defn- atfr-helper
  "String GlobalState -> GlobalState
  helper for add-to-fresult"
  [s gs]
  (if (= s "\n\n")
    (swap-state-elm! :fresult (conj (:fresult @gs) [:p ""]) gs)
    (let [fr (:fresult @gs)
          length-fresult (dec (count fr))
          length-last-el (dec (count (last fr)))
          old-last-el (last fr)
          new-last-el (if (string? (last old-last-el))
                          (assoc old-last-el length-last-el
                            (str (last old-last-el) s))
                          (conj old-last-el (str s)))
          a            (println "FR: " fr " - " (type fr))
          new-fresult (assoc fr length-fresult new-last-el)]
      (swap-state-elm! :fresult new-fresult gs))))
(let [test-db  (atom {:fresult [:div [:p "Test"] [:p "Fetter "]]})
      test-db2 (atom {:fresult [:div [:p "Test"] [:p {}]]})]
  (is (= (atfr-helper "test" test-db)
         {:fresult [:div [:p "Test"] [:p "Fetter test"]]}))
  (is (= (atfr-helper " test" test-db)
         {:fresult [:div [:p "Test"] [:p "Fetter test test"]]}))
  (is (= (atfr-helper "\n\n" test-db)
         {:fresult [:div [:p "Test"] [:p "Fetter test test"] [:p ""]]}))
  (is (= (atfr-helper "Test" test-db2)
         {:fresult [:div [:p "Test"] [:p {} "Test"]]})))
         ;; Test if last element is string or not, if not add new element

(defn add-to-fresult!
  "String -> Result
  Cosumes a string and adds it to the fresult in the app-state"
  [s]
  (atfr-helper s app-db))

(defn add-to-ignore!
  "String String -> Map
  Consumes a String ky that get's ignored and it's replacement vl,
  adds them to ignore-list
  returns the new ignore-list"
  [ky vl]
  (let [new-ignore (assoc (ignore-list) ky vl)]
    (swap-state-elm! :ignore new-ignore app-db)))

(defn set-colon!
  "Void -> GlobalState
  Sets the punc to colon in GlobalState, produces the GlobalState"
  []
  (set-punc! "." app-db))

(defn set-comma!
  "Void -> GlobalState
  Sets the punc to comma in GlobalState, produces the GlobalState"
  []
  (set-punc! "," app-db))

(defn set-dquote!
  "Void -> GlobalState
  Sets the punc to dquote in GlobalState, produces the GlobalState"
  []
  (set-punc! "\"" app-db))

(defn set-squote!
  "Void -> GlobalState
    Sets the punc to squote in GlobalState, produces the GlobalState"
  []
  (set-punc! "'" app-db))

(defn set-pgraph!
  "Void -> GlobalState
    Sets the punc to squote in GlobalState, produces the GlobalState"
  []
  (set-punc! "\n\n" app-db))

(defn set-language!
  "Keyword -> GlobalState
  Sets the language of the GlobalState to the given Keyword lan, produces the GS"
  [lan]
  (swap-state-elm! :language lan app-db))

(defn reset-punc!
  "Void -> GlobalState
  Sets the punc to colon in GlobalState, produces the GlobalState"
  []
  (set-punc! nil app-db))

(defn set-edit! [ss]
  "SwitchState -> SwitchState
  Sets the editor? according to given SwitchState, produces the new SwitchState"
  (swap-state-elm! :edit? ss app-db))

(defn set-checked! [ss]
  "SwitchState -> SwitchState
  Sets the checked? according to given SwitchState, produces the new SwitchState"
  (swap-state-elm! :checked? ss app-db))

(defn set-recording! [ss]
  "SwitchState -> SwitchState
    Sets the recording? according to given SwitchState, produces the new SwitchState"
  (swap-state-elm! :recording? ss app-db))

(defn set-fresult! [v]
  "Hiccup -> GlobalState
  Sets the final_result according to given Hiccup vector v, produces the new GlobalState"
  (swap-state-elm! :fresult v app-db))
