(ns wryte.speech-recognition
  (:require-macros [cljs.core :refer [exists?]])
  (:require [clojure.string :as s]
            [cljs.test :refer-macros [is deftest]]))

(defn log [x] (.log js/console x))

(enable-console-print!)

(def ignore_onend (atom false))

(defn setup [onstart onerror onend onresult lang]
  "Fn Fn(event) Fn Fn(event) -> Object
  Consumes the callbacks for the speech recognition and produces the speech recognition object"
  (if (not (exists? (.-webkitSpeechRecognition js/window)))
    (js/alert "Speech recognition is not present")
    (let [recognition (new js/window.webkitSpeechRecognition)]
      (set! (.-continuous recognition) true)
      (set! (.-interimResults recognition) true)
      (set! (.-onstart recognition) onstart) ;; TODO:10 onstart in Handler
      (set! (.-onerror recognition) (fn [ev]
                                      (do
                                        (reset! ignore_onend true)
                                        (onerror ev))))
      (set! (.-onend recognition) (fn [ev]
                                    (if @ignore_onend
                                      false
                                      (onend ev))))
      (set! (.-onresult recognition) onresult)
      (set! (.-lang recognition) lang)
      recognition)))

(defn set-language!
  "Object String -> Void
  Set's the given language l of the given speechRecognition sro"
  [sro l]
  (set! (.-lang sro) l))

(defn cap-sentence
  "String -> String
  Consumes a string s and produces the same string with the first word of an sentence capitlized"
  [s]
  (s/replace s #"[\.\?\!](\s+.|$)" (fn [x] (s/upper-case (first x)))))

(defn colapse-double-whitespace
  "String -> string
  Consumes a String s and produces the same string with double whitspace removed except \\n."
  [s]
  (s/replace s #"[\t\r ]{2,}" " "))

(defn colapse-linebreak
  "String -> String
  Consumes a String s and produces the same string with the linebreaks colapsed to \\n only."
  [s]
  (s/replace s #" \n|\n " "\n"))

(defn newline->paragraph
  "String -> String
  Consume a String s with newline character \\n and produces a string with a new paragraph for every newline"
  [s]
  (str "<p>" (s/replace s #"\n" "</p><p>") "</p>"))
(is (= "<p>This is a test string with one paragraph.</p>"
      (newline->paragraph "This is a test string with one paragraph.")))
(is (= "<p>This is a test string with</p><p>two paragraphs.</p>"
      (newline->paragraph "This is a test string with\ntwo paragraphs.")))

(defn sanitize [s]
  "String -> String
  Consumes a string use cap-sentence and colapse-double-whitespace on the string."
  [s]
  (-> s cap-sentence colapse-double-whitespace colapse-linebreak))


#_
(fn [txt] (str (.toUpperCase (.charAt txt 0) (.toLowerCase (.substr txt 1)))))

#_
(is (= "Right sentence. Rigth sentence! Right Sentence?"
       (cap-sentence "right sentence. rigth sentence! right Sentence?")))
;(cap-sentence "right sentence. rigth sentence! right Sentence?")
(println (cap-sentence "right sentence. rigth sentence! right Sentence?"))
(println (cap-sentence "Mal sehen was jetzt so passiert. das jetzt einfach anders ist was ich jetzt nicht verstehe ist wieso jetzt nicht mehr ersatz punkt punkt punkt punkt"))

(defn stop-webspeech
  "Object -> Void
  Consumes a webspeech object, stops the speech-recognition"
  [ws]
  (do
    (.stop ws)))

(defn start-webspeech
  "Object -> Void
  Consumes a webspeech object, starts the speech-recognition"
  [ws]
  (do
    (.start ws)))
