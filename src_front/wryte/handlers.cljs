(ns wryte.handlers
    (:require
              [wryte.db :as db :refer [app-db webspeech]]
              [wryte.speech-recognition :as sapi]
              [reagent.core :as reagent]
              [cljs.nodejs :as nodejs]
              [ajax.core :refer [GET POST]]
              [goog.string :as string]
              [goog.events :as events]
              [clojure.string :refer [join split-lines split lower-case]]
              [tubax.core :refer [xml->clj]]
              [tubax.helpers :as th]
              [cljs.test :refer-macros [is deftest]]
              [hickory.core :as h]
              [hickory.convert :as convert]
              [hickory.select :as s]
              [hickory.zip :as zip]
              [clojure.zip :as czip]))

(defn log [x] (.log js/console x))

(def remote (nodejs/require "remote"))
(def MENU (.require remote "menu"))
(def MENUITEM (.require remote "menu-item"))

;; ========
;; Helpers:

(defn stop-webspeech
  []
  (do
    (db/set-checked! false)
    (sapi/stop-webspeech @webspeech)
    ;; Wait till webspeech finished chanel
    ;; then send to languagetool
    ;; Wait till response
    ;; change fresult
    (db/set-recording! false)))

(defn start-webspeech
  []
  (do
    (sapi/start-webspeech @webspeech)
    (db/set-recording! true)))

(defn correct-error
  "String Error -> String
  Consumes a String S and and associated Error, produces a new String with the
  Error corrected" ;; WONTFIX needs work for replacements, that have a different length
  [s e]
  (let [attr (:attributes e)
        row  (int (:fromy attr))
        col  (int (:fromx attr))
        end (int (:tox attr))
        replacement (first (.split (:replacements attr) "#"))
        lines (split-lines s)
        line (nth lines row)
        newline (str (subs line 0 col) replacement (subs line end))
        newlines (concat (conj (subvec lines 0 row) newline) (subvec lines (inc row)))
        result (join "\n" newlines)]
    result))

(defn pos-at-line-helper
  [cv los c]
  (let [newc (+ c (inc (count (first los))))]
    (if (empty? los)
      cv
      (pos-at-line-helper (conj cv newc) (rest los) newc))))
(is (= (pos-at-line-helper [0] ["1234" "678" "10"] 0) [0, 5, 9, 12]))

(defn pos-at-line
  "String -> VecOfInt
  Consumes a String s and produces a accumulated ListOfInt with char count a
  beginning of line"
  [s]
  (let [lines (split s #"\n")]
    (pos-at-line-helper [0] lines 0)))
(is (= (pos-at-line "1234\n678\n10") [0, 5, 9, 12]))


(def loe1 [{:attributes {:fromy 0 :fromx 7 :toy 0 :tox 10 :replacements "Text#Test" :errorlength 4
                         :context "Dast ist der Context" :contextoffset 2}}
           {:attributes {:fromy 0 :fromx 16 :toy 0 :tox 20 :replacements "zwo#zwei" :errorlength 4
                         :context "Dast ist der Context" :contextoffset 2}}
           {:attributes {:fromy 0 :fromx 29 :toy 0 :tox 35 :replacements "" :errorlength 6
                         :context "Dast ist der Context" :contextoffset 2}}])
(def loe2 [{:attributes {:fromy 0 :fromx  5 :toy 0 :tox  8 :replacements "mit" :errorlength 3}}])
(def s1 "Dieser text hat Zwei Fehler. OnVista ist Testsieger.")
(def s2 "Dieser Text hat zwo Fehler. OnVista ist Testsieger.")
(def s3 "Text müt\nAbsatz.")
(def los1 ["Dieser " " hat " " Fehler. OnVista ist Testsieger."])
(def los2 ["Text" "zwo"])
(def los3 ["Dieser " "Text" " hat " "zwo" " Fehler. OnVista ist Testsieger."])

(defn split-at-errors
  "String ListOfErrors ListOfInt Int Int -> ListOfString
  consume a string s and a ListOfErrors loe and ListOfInt with cummulated line-counts lp
  produces a ListOfString with s split a the errors (without the errors)."
  [s loe lp start]
  (if (empty? loe)
    (cons (subs s start) loe)
    (let [attr (:attributes (first loe))
          row  (int (:fromy attr))
          fromx  (int (:fromx attr))
          tox (int (:tox attr))
          length (int (:errorlength attr))
          end (+ (nth lp row) fromx)
          new-start (+ end length)
          word (subs s (+ (nth lp row) fromx ) (+ (nth lp row) fromx 1 length))
          replacement (first (.split (:replacements attr) "#"))]
      ; (println "Row: " row)
      ; (println "fromx: " fromx)
      ; (println "tox: " tox)
      ; (println "length: " length)
      ; (println "end: " end)
      ; (println "new-start: " new-start)
      ; (println "replacement:" replacement)
      ; (println (subs s start end))
      ;(println "word:" word)
      ; (println)
      ;(if (or (> (count replacement) 0))
      (cons (subs s start end) (split-at-errors s (rest loe) lp new-start)))))
      ;  (split-at-errors s (rest loe) lp start))))
(is (= (split-at-errors s1 loe1 (pos-at-line s1) 0) los1))

(defn corrections
  "ListOfErrors -> ListOfStrings
  Consumes a ListOfErrors and Produces the replecements if present"
  [loe]
  (let [correction (fn [e]
                      (let [attr (:attributes e)
                            context (:context attr)
                            offset (int (:contextoffset attr))
                            length (int (:errorlength attr))
                            word (lower-case (subs context offset (+ offset length)))
                            replacement
                            (first (.split (:replacements attr) "#"))
                            alternatives
                            (rest (split (:replacements attr) "#"))
                            rule-id (:ruleId attr)]
                        (cond (contains? (db/ignore-list) word)
                              (str "<span class='ignored' data-replaced='" word "'>" (get (db/ignore-list) word) "</span>")
                              (or (= rule-id "UPPERCASE_SENTENCE_START") (= rule-id "WHITESPACE_RULE"))
                              (str "<span class='uppercase-sentence-start' data-alternatives='" (clj->js alternatives) "' data-replaced='" word "'>" replacement "</span>")
                              :else (if (> (count replacement) 0)
                                      (str "<span class='error' data-alternatives='" (clj->js alternatives) "' data-replaced='" word "'>" replacement "</span>")
                                      (str "<span class='error' data-alternatives='" (clj->js alternatives) "' data-replaced='" word "'>" word "</span>")))))]
      (filter (fn [x] x) (map correction loe))))
(is (= (corrections loe1) los2))

(defn zip-corrections
  "ListOfString ListOfStrings -> ListOfStrings
  Produces a zipped ListOfString"
  [los1 los2]
  (if (= (count los1) (count los2))
     (interleave los1 los2)
     (conj (vec (interleave los1 los2)) (last los1))))
(is (= (zip-corrections los1 los2) los3))
(is (= s2 (join "" (zip-corrections los1 los2))))

(defn correct-errors
  "String ListOfErrors -> ListOfString
  consume a string s and a ListOfErrors loe and produces a ListOfString with
  s split a the errors (and the correction) in between the splits."
  [s loe]
  (let [line-positions (pos-at-line s)
        splits (split-at-errors s loe line-positions 0)
        corrected (corrections loe)
        zipped (zip-corrections splits corrected)]
    ; (if (= count(corrections) count(corrections))
    ;   (interleave splits))))
      (println zipped)
      (log (join "" zipped))
      (join "" zipped)))

(defn ltool-handler
  "(String) Response -> Void
  Get's the response from languagetool and applys the result to fresult"
  [r]
  (let [errors (th/find-all (xml->clj r) {:tag :error})
        ;;corrected-result (reduce correct-error (db/fresult) errors)
        corrected-result (correct-errors (db/fresult->string (db/fresult)) errors)
        e (print (str "COR: " corrected-result))
        paragraphed (sapi/newline->paragraph corrected-result)
        hiccuped (map h/as-hiccup (h/parse-fragment paragraphed))]
    (log (str "RESULT: " r))
    (println "HICCUPED: " hiccuped)
    (println "LTOOL-HANDLER: " (db/set-fresult! (vec (cons :div hiccuped))))))

(defn ltool-error-handler
  []
  (log "Couldn't get response from languagetool.org"))

(defn send-to-languagetool
  "String -> Void
  Sends string to languagetool.org"
  [s]
  (do
    (log (str "SEND: " s))
    (GET "https://languagetool.org:8081"
      {:params {:language (db/locale) :text (string/urlEncode s)}
       :handler ltool-handler :error-handler ltool-error-handler
       :b (string/urlEncode "enabled")})))

(defn toggle-recording!
  [event]
  (if (db/recording?)
    (stop-webspeech)
    (do
      (.start @webspeech)
      (db/set-recording! true))))

(defn add-alternatives!
  "ListOfString Node Object Function -> Void
  Consumes a list of strings los, a Node el and adds each item of los
  to the delivered context menu with the click-handler Function f"
  [los el menu f]
  (if (empty? los)
    true
    (do
      (.append menu (new MENUITEM (clj->js {:label (first los)
                                            :click #(f (first los) el)})))
      (add-alternatives! (rest los) el menu f))))

;; Stub
(defn override-correction!
  "String Node -> GlobalState
  Interchenges innerText of Node el with String w, replaces the GlobalState"
  [w el]
  (let [c (.-innerText el)
        replaced (.-replaced (.-dataset el))
        alternatives (.-alternatives (.-dataset el))
        fresult-hkr (first (convert/hiccup-fragment-to-hickory [(db/fresult)]))
        slt (s/select-locs (s/child
                            (s/tag :div) (s/tag :p) (s/and
                                                      (s/attr :data-replaced #(= % replaced))
                                                      (s/attr :data-alternatives #(= % alternatives))
                                                      (s/find-in-text (re-pattern c))))
                     fresult-hkr)
        loc (first slt)
        loc-el (first loc)
        old-attrs (:attrs loc-el)
        new-attrs (assoc old-attrs :data-alternatives "" :class "ignored")
        new-loc-el (assoc loc-el :attrs new-attrs :content [w])
        new-loc (czip/replace loc new-loc-el)
        new-fresult (convert/hickory-to-hiccup (czip/root new-loc))]
    ;;(println fresult-hkr)
    ; (println (type (first (first slt))))
    ; (println (first slt))
    ; (println (czip/replace (first slt) "eniatrentirane"))))
    (println (convert/hickory-to-hiccup (czip/root new-loc)))
    (db/set-fresult! new-fresult)))
false

(defn add-to-ignore-list!
  "String String -> GlobalState
  Add the ignored String i with the replaced String r, returns the GlobalState"
  [i r]
  (do
    (db/add-to-ignore! i r)
    (println (str "IGNORE: " (db/ignore-list)))))

;; ==========
;; Functions:

(defn on-click-dict-button
  [event]
  (cond (= (db/edit?) true)
       (do
         (db/set-edit! false))))

(defn on-click-edit-button
  [event]
  (cond (= (db/edit?) false)
        (do
          (db/set-edit! true)
          (cond
           (db/recording?)
           (stop-webspeech)))))

(defn on-click-start-button
  [event]
  (toggle-recording! event))

(defn on-click-colon
  "Event -> Void
  Adds a colon to the dictation text"
  [event]
  (do
    (.stop @webspeech)
    (db/set-colon!)))

(defn on-click-comma
  "Event -> Void
  Adds a colon to the dictation text"
  [event]
  (do
    (.stop @webspeech)
    (db/set-comma!)))

(defn on-click-dquote
  "Event -> Void
  Adds a colon to the dictation text"
  [event]
  (do
    (.stop @webspeech)
    (db/set-dquote!)))

(defn on-click-squote
  "Event -> Void
  Adds a colon to the dictation text"
  [event]
  (do
    (.stop @webspeech)
    (db/set-squote!)))

(defn on-click-pgraph
  "Event -> Void
  Adds a colon to the dictation text"
  [event]
  (do
    (.stop @webspeech)
    (db/set-pgraph!)))

(defn on-click-dropdown
  [lg]
  (println (lg db/languages)))

(defn on-click-deutsch
  "Event -> Void
  Sets the speech reconigtion and spell checking to deutsch"
  [event]
  (do
    (println (db/language))
    (db/set-language! :deutsch)))

(defn on-click-english
  "Event -> Void
  Sets the speech reconigtion and spell checking to English"
  [event]
  (do
    (println (db/language))
    (db/set-language! :english)))

(defn on-click-context-menu-item
  "String Node -> Void
  Consumes the chosen word w and concerning element el and replaces the
  content of the element with w, additionally adds the chosen word to the
  ignore list with for content of the element"
  [w el]
  (let [c (.-innerText el)]
    (add-to-ignore-list! c w)
    (override-correction! w el)
    (log (str "Replaced " c " with " w "."))))

(defn on-change-language-chooser
  "Event -> Void
  Changes the language to the chosen value"
  [event]
  (let [lan (-> event .-target .-value)]
    (println  lan)
    (db/set-language! (keyword lan))
    (sapi/set-language! @webspeech (db/locale))
    (println (db/locale))))

(defn on-start-recog
  []
  (do
    (swap! app-db assoc :recording true)
    (cond
      (not= (last (db/fresult)) [:p ""]) (db/add-to-fresult! " "))
    (println "SPEAK NOW!")))

(defn on-error-recog [event]
  (do
    ;; (swap! state assoc :ignore_onend true))
    (if (= (.-error event) "no-speech")
      (println "NO SPEECH"))
      ;; TODO genauere fehlerbehandlung
    (if (= (.-error event) "audio-capture")
      (println "NO MICROPHONE"))
    (if (= (.-error event) "not-allowed")
      (println "NOT ALLOWED"))
    (println (str "speechRecognitionError: " (.-error event)))))

(defn on-end-recog [ev]
  (cond
    (and (db/recording?) (db/punc))
    (do
      (log (str "Recording: " (db/recording?) " Punctuation: " (db/punc)))
      (db/add-to-fresult! (db/punc))
      (db/reset-punc!)
      (println (db/punc))
      (.start @webspeech))
    (not (db/checked?))
    (send-to-languagetool (db/fresult->string (db/fresult)))))

(defn on-result-recog [event]
  (let [results (.-results event)
        length (.-length results)
        i (atom (.-resultIndex event))]
    (swap! app-db assoc :iresult "")
    (while (< @i length)
      (if (.-isFinal (aget results @i))
        (db/add-to-fresult! (sapi/sanitize
                             (.-transcript (aget (aget results @i) 0))))
        (swap! app-db assoc :iresult (sapi/sanitize (str (:iresult @app-db)(.-transcript (aget (aget results @i) 0))))))
      (swap! i inc))))
    ;(println "INTERIM " (:iresult @app-db))
    ;(println "FINAL " (:fresult @app-db))))

(defn on-blur-editor
  "Changes final_result to user input"
  [event]
  (let [innerHTML (-> event .-target .-innerHTML)
        hiccup    (vec (first (map h/as-hiccup (h/parse-fragment innerHTML))))]
    (println hiccup)
    (db/set-fresult! hiccup)))

(defn handle-keydown
  "KeyEvent -> GlobalState
  Handles the keyevents that are created by js/document"
  [ev]
  (let [mac? (if (= (.-platform js/process) "darwin") true false)
        ky  #(.-keyCode %)
        ctrl? (if mac? #(.-metaKey %) #(.-ctrlKey %))]
    (do
      (cond
        (and (not (db/edit?)) (= (ky ev) 32)) (toggle-recording! ev)
        (db/recording?)
        (cond
         (= (ky ev) 13)
         (do
            (.stop @webspeech)
            (db/set-pgraph!)
            (.preventDefault ev)) ;; Enter
         (or (= (ky ev) 38) (= (ky ev) 190))
         (do (.stop @webspeech) (db/set-colon!))   ;; UpArrow/Colon
         (or (= (ky ev) 40) (= (ky ev) 188))
         (do (.stop @webspeech) (db/set-comma!))  ;; DownArrow/Comma
         (or (= (ky ev)  9) (and (= (ky ev) 188) (.-shift ev)))
         (do (.stop @webspeech) (db/set-dquote!)))))))  ;; TAB/"

(defn handle-click
  "ClickEvent -> GlobalState
  Handles the clickevents that are created by js/document"
  [ev]
  (let [el (.-target ev)
        className (.-className el)]
    (if-not (and (= "SPAN" (.-nodeName el)) (contains? #{"error" "ignored"} className))
     false
     (do
      (log ev)
      (let [menu (new MENU)]
        (when (= className "error")
          (.append menu (new MENUITEM (clj->js {:label "Alternatives" :enabled false})))
          (add-alternatives! (split (.-alternatives (.-dataset el)) #",") el
                            menu on-click-context-menu-item)
          (.append menu (new MENUITEM (clj->js {:type "separator"}))))
        (.append menu (new MENUITEM (clj->js {:label "Replaced" :enabled false})))
        (.append menu (new MENUITEM (clj->js {:label (.-replaced (.-dataset el))
                                              :click #(on-click-context-menu-item (.-replaced (.-dataset el)) el)})))
        (.popup menu (.getCurrentWindow remote)))))))

(defn register-keyevents
  "Register the keyhandlers"
  []
  (events/listen js/window "keydown" handle-keydown))

(defn register-mousevents
  "Register the moushandlers"
  []
  (events/listen js/window "click" handle-click))


(defn setup-webspeech []
 (let [recog (sapi/setup on-start-recog on-error-recog on-end-recog on-result-recog
              (db/locale))]
  (reset! webspeech recog)))

(defn setup-editor []
  true)
