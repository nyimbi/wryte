# wryte

> Write Like You Talk
[Paul Graham](http://www.paulgraham.com/talk.html)

wryte is a dictation software for blogger and other webworkers. Features are:

- automatic spell checking
- easy punctuation via shortcuts, for a natural dictaion flow
- easily edit and copy text after dictation

![wryte after dictation](https://bytebucket.org/marco_rockiger/wryte/raw/f344b1730ab0f1fd8e42274b02b224b9e3bd3eca/wryte.jpg)

## Shortcuts for dictation

wryte supplys shortcuts that insert certain punctuation during the dictation:

- <kbd>Space</kbd> - start/stop dictation
- <kbd>,</kbd> - comma
- <kbd>.</kbd> - period
- <kbd>"</kbd> - double quotation
- <kbd>'</kbd> - single quotation
- <kbd>Enter</kbd> - paragraph

## Technologies used

- electron
- clojurescript
- reagent
- https://languagetool.org
- Speech-To-Text-Api in electron

## Known Issues

- Sometimes app sets more spaces after a period than necessary.
- On Arch-Linux with Xfce and Pulse audio input sometimes collapses and no voice comes thru.
- Strange things happen sometimes with correction of first word in sentence.

## License

MIT License

Copyright (c) 2016 Marco-Alexander Laspe <marco@rockiger.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
