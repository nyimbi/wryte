Projektvorstellung wryte

wryte ist eine kleine Diktier-App, die ich geschrieben habe. Die Grundidee ist, dass man am besten so schreibt, wie man spricht. Was liegt da also näher, als wirklich zu sprechen.

Umgesetzt habe ich breit mit Elektron auf Basis der Speech-To-Text-API von Google Chrome. Da es beim Diktieren häufig zu kleinen Fehlern kommt, hatte ich die Idee, den diktierten Text im Anschluss an das Diktat durch eine Rechtschreibprüfung zu jagen.

Um das Diktieren flüssiger zu gestalten, können Satzzeichen einfach per Shortcut eingegeben werden und müssen nicht diktiert werden. So werden Gedankengänge nicht unnötig unterbrochen.

Wenn man mit dem Diktieren fertig ist, kommt man in einen Edit-Modus, in dem einem alle gefundenen Fehler angezeigt werden, und man den Text schnell überarbeiten kann.

Wichtig: Die ganze App ist ein erster Prototyp, der noch viele kleine Bugs besitzt und dem noch grundlegende Funktion fehlen, wie zum Beispiel eine Speichern-Funktion.

Umgesetzt habe ich wryte mit folgenden Technologien:
- Elektron
- Language-Tool
- ClojureScript

Ihr findet den Source-Code unter: . .

Downloads gibt es für

- Linux
- MacOS  
- Windows

Wer Lust das hat wryete zu testen, kann mir gerne Feedback zur Funktionsweise geben.
